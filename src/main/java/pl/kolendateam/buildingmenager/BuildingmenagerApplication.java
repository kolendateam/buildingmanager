package pl.kolendateam.buildingmenager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildingmenagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildingmenagerApplication.class, args);
	}
}
