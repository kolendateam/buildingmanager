package pl.kolendateam.buildingmenager.user;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoder {

    public String encode(String password){
        return new BCryptPasswordEncoder().encode(password);
    }
}
