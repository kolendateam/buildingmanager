package pl.kolendateam.buildingmenager.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserApi {

    UserService userService;

    @Autowired
    public UserApi(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/api/public")
    public String getMessage1() {
        this.userService.createUser();
        return "Hello from public API controller";
    }

    @GetMapping("/api/private")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public String getMessage2() {
        return "Hello from private API controller";
    }

//    @PostMapping("login")
//    public UserDTO login(@RequestBody @Valid LoginForm loginForm){
//        return userService.login(loginForm);
//    }
}
