package pl.kolendateam.buildingmenager.user.form;

import javax.validation.constraints.NotEmpty;

public class LoginForm {
    @NotEmpty
    private String login;
    @NotEmpty
    private String password;

    public LoginForm() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
